#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-06-30
'''

from django.conf.urls import url

from apps.authx.views import LoginUser, logoutUser

urlpatterns = [
    url(r'^login/$', LoginUser.as_view(), name='loginurl'),
    url(r'^logout/$', logoutUser, name='logouturl'),
]
