#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-06-30
'''

from django.shortcuts import render

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from apps.app_user.models import UserRole


class LoginUser(APIView):
    '''
    登录用户
    '''
    def get(self, request, format=None):
        return render(request, 'user/login.html')

    def post(self, request, format=None):
        post_parmas = request.POST
        username = post_parmas['username']
        password = post_parmas['password']
        if not User.objects.filter(username=username).exists():
            return Response({'msg': '用户不存在', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if user is None:
            return Response({'msg': '密码错误', 'errorCode': '401'}, status=status.HTTP_401_UNAUTHORIZED)
        login(request, user)
        return Response({'status': 'success'}, template_name='home/index.html')

@login_required
def logoutUser(request):
    '''
    退出当前用户
    '''
    logout(request)
    return render(request, 'user/login.html')
