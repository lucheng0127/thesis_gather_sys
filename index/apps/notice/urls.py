#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-07-02
'''

from django.conf.urls import url

from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView

urlpatterns = [
    url(r'^home/$', login_required(TemplateView.as_view(template_name='home/index.html')), name='index'),
]
