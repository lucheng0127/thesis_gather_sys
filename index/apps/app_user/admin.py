#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-06-30
'''

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import UserRole


class UserRoleInline(admin.StackedInline):
    model = UserRole
    can_delete = False
    verbose_name_plural = 'user_role'

class UserAdmin(BaseUserAdmin):
    inlines = (UserRoleInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
