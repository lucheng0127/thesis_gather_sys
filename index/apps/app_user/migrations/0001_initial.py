# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-06-30 01:42
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserRole',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username_cn', models.CharField(max_length=50)),
                ('role', models.CharField(choices=[('admin', '管理员'), ('leader', '院领导'), ('dep_leader', '系主任'), ('teacher', '老师')], default='teacher', max_length=20)),
                ('department', models.CharField(blank=True, choices=[('CE', '通信工程'), ('ICS', '信息与计算科学')], max_length=5)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
