#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-06-30
'''

from django.db import models
from django.contrib.auth.models import User

class UserRole(models.Model):
    ROLE_TYPE = (
        ('admin', u'管理员'),
        ('leader', u'院领导'),
        ('dep_leader', u'系主任'),
        ('teacher', u'老师'),
    )

    DEP_TYPE = (
        ('CE', u'通信工程'),
        ('ICS', u'信息与计算科学'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='userrole')
    username_cn = models.CharField(max_length=50)
    role = models.CharField(max_length=20, choices=ROLE_TYPE, null=False, default='teacher')
    department = models.CharField(max_length=5, choices=DEP_TYPE, null=False, blank=True)

    def __unicode__(self):
        return self.user.username
