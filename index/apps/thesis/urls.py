#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-07-02
'''

from django.conf.urls import url

from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView

from apps.thesis.views.teacher_thesis_views import TeacherThesis

urlpatterns = [
    url(r'^add_thesis/$', login_required(TemplateView.as_view(template_name='thesis/add_thesis.html')), name='thesis_list'),
    url(r'^teacher_thesis/$', login_required(TeacherThesis.as_view()), name='teacher_thesis'),
]
