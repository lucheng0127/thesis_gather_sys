#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-07-02
'''
import re
import os
import shutil

from django.conf import settings
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from rest_framework.decorators import APIView
from rest_framework.response import Response
from rest_framework import status

from apps.thesis.models import Thesis


class TeacherThesis(APIView):
    """
    GET方法获取当前用户指导的所以毕业设计，POST批量导入毕业设计
    """
    def get(self, request, format=None):
        user = request.user
        pass

    def post(self, request, format=None):
        #初始化函数所需数据
        user = request.user
        post_parmars = request.POST
        data_lines = post_parmars.get('data').splitlines()
        goodData = []
        badData = []
        add_parmars = {}
        add_parmars['teacher'] = user
        #检测数据里面有没有非法字符防止XSS攻击


        #对每行数据进行分割等处理
        for line in data_lines:
            data1 = line.split(',')
            if len(data1) != 4:
                badData.append(line)
                continue

            data2 = [re.sub(u'[\s+]', u'', d) for d in data1]
            data3 = [re.sub(u'[()]', u'', d) for d in data2]
            data4 = [re.sub(u'[（）]', u'', d) for d in data3]
            data = [re.sub(u'[_]', u'', d) for d in data4]
            add_parmars['stu_name'], add_parmars['stu_num'], add_parmars['year'], add_parmars['title'] = data

            # 验证填入的数据是否符合格式要求

            thesis = Thesis(**add_parmars)
            try:
                # 创建学生毕业设计文件夹
                p = u'{0}{1}'.format(thesis.stu_name, thesis.num)
                file_path = thesis.file_path
                if os.path.isdir(file_path):
                    # 添加日志
                    badData.append(line)
                    continue
                thesis_base_path = os.path.join(settings.BASE_DIR, u'media', u'.thesis_base')
                shutil.copytree(thesis_base_path, file_path)
                thesis.save()
                # 添加日志
                goodData.append(line)
            except:
                badData.append(line)

        all_num = len(data_lines)
        success_num = len(goodData)
        failed_num = len(badData)
        return Response({'all_num': all_num, 'success_num': success_num, 'failed_num': failed_num},status=status.HTTP_200_OK)
