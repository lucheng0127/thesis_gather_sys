from django.contrib import admin

from apps.thesis.models import Thesis

admin.site.register(Thesis)
