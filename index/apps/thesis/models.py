#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-06-30
'''

import os
import time

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User

class Thesis(models.Model):
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, related_name='theses') #指导老师
    stu_name = models.CharField(max_length=50, null=False, blank=True) #学生中文姓名
    stu_num = models.CharField(max_length=11, null=False, blank=True, unique=True) #学生学号
    title = models.CharField(max_length=100, null=False, blank=True) #论文题目
    num = models.CharField(max_length=50, null=False, blank=True, default=str(int(time.time()))) #论文编号，防止猜到文件存储结构造成资料泄露
    year = models.IntegerField(blank=True, null=True) #毕业年份

    def __unicode__(self):
        return self.stu_name

    def _get_department(self):
        '''
        获取该论文所属的专业
        '''
        return self.teacher.userrole.department

    def _get_teacher_name(self):
        return self.teacher.username

    def _get_file_path(self):
        return os.path.join(settings.BASE_DIR, u'media', u'thesis',
        self.year, self.department, self.teacher_name, self.stu_num)
    department = property(_get_department) #所属专业
    teacher_name = property(_get_teacher_name) #指导老师username
    file_path = property(_get_file_path) #文件存储路径
