/*Post data with Ajax*/

function csrf2ajax() {
    jQuery(document).ajaxSend(function(event, xhr, settings) {
       function getCookie(name) {
           var cookieValue = null;
           if (document.cookie && document.cookie !=='') {
               var cookies = document.cookie.split(';');
               for (var i = 0; i < cookies.length; i++) {
                   var cookie = jQuery.trim(cookies[i]);//去掉cookies[i]起始和结尾的空格
                   if (cookie.substring(0, name.length + 1) === (name + '=')) {
                       cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                       break;
                   }
               }
            }
            return cookieValue;
       }

       // function sameOrigin(url) {
       //     //绝对路径或者相对路径
       //     var host = document.location.host; //host + post
       //     var protocol = document.location.protocol;
       //     var sr_origin = '//' + host;
       //     var origin = protocol + sr_origin;
       //     return (url === origin || url.slice(0, origin.length + 1) === origin + '/') ||
       //      (url === sr_origin || url.slice(0, sr_origin.length + 1) === sr_origin + '/') ||
       //      // or any other URL that isn't scheme relative or absolute i.e relative.
       //      !(/^(\/\/|http:|https:).*/.test(url));
       // }

       function safeMethod(method) {
           return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
       }

       if (!safeMethod(settings.type)) {
           xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
       }
       // if (!safeMethod(settings.type) && sameOrigin()) {
       //     xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
       // }
    });
}

function g2url(url) {
    var host = document.location.host; //host + post
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin + url;
    window.location.href = origin;
}

var gpData = {
    ajaxDbFormat: function(obj) {
        var ajaxMap = {};
        ajaxMap.timeout = 30000;
        ajaxMap.type = obj.type || 'GET';
        ajaxMap.url = obj.url || '';
        ajaxMap.async = obj.async || true;
        ajaxMap.data = obj.data || '';
        ajaxMap.dataType = obj.dataType || 'json';
        ajaxMap.beforeSend = obj.beforeSend || '';
        ajaxMap.success = obj.success || '';
        ajaxMap.error = obj.error || '';
        ajaxMap.complete = obj.complete || '';
        return ajaxMap
    },
    ajax: function(obj) {
        var ajaxObj = this.ajaxDbFormat(obj);
        if (!ajaxObj.url) {
            console.error("Request address not found!");
            return false
        }
        csrf2ajax();
        $.ajax({
            type: ajaxObj.type,
            url: ajaxObj.url,
            data: ajaxObj.data,
            dataType: ajaxObj.dataType,
            async: ajaxObj.async,
            timeout: ajaxObj.timeout,
            beforeSend: function(jqXHR) {
                ajaxObj.beforeSend ? window[ajaxObj.beforeSend](jqXHR, obj) : ''
            },
            success: function(data) {
                ajaxObj.success ? window[ajaxObj.success](data, obj) : ''
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ajaxObj.error ? window[ajaxObj.error](jqXHR, textStatus, errorThrown, obj) : ''
            },
            complete: function(jqXHR, textStatus) {
                ajaxObj.complete ? window[ajaxObj.complete](jqXHR, textStatus, obj) : ''
            }
        })
    }
};
