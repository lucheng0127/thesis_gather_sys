#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-06-30
'''

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from django.views.generic.base import TemplateView

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='user/login.html'), name='homePage'),
    url(r'^', include('apps.authx.urls')),
    url(r'^', include('apps.notice.urls')),
    url(r'^thesis/', include('apps.thesis.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
